import { takeEvery, takeLatest } from 'redux-saga/effects';
import { loginRequest } from './auth/actions';
import { handleLogin } from './auth/sagas';

export default function* rootSaga() {
  yield takeLatest(loginRequest.type, handleLogin);
}
