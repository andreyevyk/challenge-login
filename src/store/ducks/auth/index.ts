import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';
import { loginError, loginRequest, loginSuccess, logout } from './actions';
import { AuthState, User } from './types';

const initialState: AuthState = {
  isAuthenticated: false,
  user: null,
  error: '',
};

export default createReducer(initialState, {
  [loginSuccess.type]: (_, action: PayloadAction<User>) => {
    return {
      isAuthenticated: true,
      user: action.payload,
    };
  },
  [HYDRATE]: (state, action: PayloadAction<any>) => {
    return {
      ...state,
      ...action.payload,
    };
  },
  [loginRequest.type]: state => {
    return {
      ...state,
    };
  },
  [loginError.type]: (state, action: PayloadAction<string>) => {
    return {
      ...state,
      error: action.payload,
    };
  },
  [logout.type]: () => {
    return {
      isAuthenticated: false,
      user: null,
    };
  },
});
