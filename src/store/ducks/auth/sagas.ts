import { AxiosResponse } from 'axios';
import { call, put } from 'redux-saga/effects';
import http from '../../../config/http';
import { loginSuccess, loginError } from './actions';
import { User } from './types';

function requestGetUsers() {
  return http.get<User[]>('users');
}

export function* handleLogin(action) {
  const httpRequest = action.payload;
  try {
    const response: AxiosResponse<User[]> = yield call(requestGetUsers);

    const { data: users } = response;

    const user = users.find(u => u.email === httpRequest.email);
    if (!user) {
      throw 'Usuário não existe';
    }
    localStorage.setItem('user', JSON.stringify(user));
    yield put(loginSuccess(user));
  } catch (error) {
    yield put(loginError(error));
  }
}
