export interface Auth {
  email: string;
  password: string;
}

export interface User {
  id: string;
  avatar: string;
  name: string;
  email: string;
  createdAt: Date;
}

export interface AuthState {
  isAuthenticated: boolean;
  user: User;
  error: string;
}
