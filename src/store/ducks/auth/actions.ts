import { createAction } from '@reduxjs/toolkit';
import { Auth, User } from './types';

export const loginRequest = createAction<Auth>('auth/loginRequest');
export const loginSuccess = createAction<User>('auth/loginSuccess');
export const loginError = createAction<string>('auth/loginError');

export const logout = createAction('auth/logout');
