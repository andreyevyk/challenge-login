import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { createWrapper } from 'next-redux-wrapper';
import rootSaga from './ducks/rootSaga';

import reducer from './ducks/rootReducer';

const initStore = () => {
  const sagaMiddleware = createSagaMiddleware();

  const store = configureStore({
    reducer,
    middleware: [...getDefaultMiddleware({ thunk: false }), sagaMiddleware],
  });
  sagaMiddleware.run(rootSaga);

  return store;
};

export const wrapper = createWrapper(initStore);
