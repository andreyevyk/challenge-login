import Head from 'next/head';

interface SEOProps {
  title: string;
  description?: string;
  shouldIndexPage?: boolean;
}

export default function SEO({
  title,
  description,
  shouldIndexPage = true,
}: SEOProps): JSX.Element {
  return (
    <Head>
      <title>{title}</title>

      {description && <meta name="description" content={description} />}

      {!shouldIndexPage && <meta name="robots" content="noindex,nofollow" />}

      <meta httpEquiv="x-ua-compatible" content="IE=edge,chrome=1" />
      <meta name="MobileOptimized" content="320" />
      <meta name="HandheldFriendly" content="True" />
      <meta name="theme-color" content="#121214" />
      <meta name="msapplication-TileColor" content="#121214" />
      <meta name="referrer" content="no-referrer-when-downgrade" />
      <meta name="google" content="notranslate" />

      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:locale" content="pt_BR" />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content={title} />

      <meta name="twitter:title" content={title} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@wiseupbr" />
      <meta name="twitter:creator" content="@wiseupbr" />
    </Head>
  );
}
