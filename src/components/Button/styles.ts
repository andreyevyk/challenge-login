import styled from 'styled-components';

export const Container = styled.button`
  width: 100%;
  border: 0;
  height: 50px;

  color: #fff;
  font-weight: 500;
  text-transform: uppercase;

  background: linear-gradient(267.79deg, #383e71 0%, #9d25b0 99.18%);
  box-shadow: 0px 10px 25px #cf99db;
  border-radius: 8px;
`;
