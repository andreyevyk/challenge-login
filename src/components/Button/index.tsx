import { InputHTMLAttributes, ReactNode } from 'react';
import { CSSProperties } from 'styled-components';
import { Container } from './styles';

interface ButtonProps extends InputHTMLAttributes<HTMLButtonElement> {
  children: ReactNode;
  style?: CSSProperties;
}

const Button = ({ children, style }: ButtonProps): JSX.Element => {
  return <Container style={style}>{children}</Container>;
};

export default Button;
