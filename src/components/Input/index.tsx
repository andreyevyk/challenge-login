import { InputHTMLAttributes, useCallback, useState } from 'react';
import { Container, Label, Error, Wrapper } from './styles';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  containerStyle?: React.CSSProperties;
  name: string;
  error?: string;
  label?: string;
}

const Input = ({
  containerStyle,
  name,
  label,
  error,
  defaultValue,
  value,
  onChange,
  ...rest
}: InputProps): JSX.Element => {
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);

  const handleInputBlur = useCallback(() => {
    setIsFocused(false);

    setIsFilled(!!value);
  }, [value]);

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  return (
    <Wrapper>
      {!!label && <Label htmlFor={name}>{label}</Label>}
      <Container
        style={containerStyle}
        isErrored={!!error}
        isFocused={isFocused}
        isFilled={isFilled}
      >
        <input
          name={name}
          id={name}
          value={value}
          onChange={onChange}
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
          defaultValue={defaultValue}
          style={{}}
          {...rest}
        />
      </Container>
      {!!error && <Error>{error}</Error>}
    </Wrapper>
  );
};

export default Input;
