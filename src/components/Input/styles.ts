import styled, { css } from 'styled-components';

interface ContainerProps {
  isFilled: boolean;
  isFocused: boolean;
  isErrored: boolean;
}

export const Wrapper = styled.div`
  width: 100%;
  & + div {
    margin-top: 10px;
  }
`;
export const Container = styled.div<ContainerProps>`
  border-radius: 8px;
  padding: 15px;
  width: 100%;

  display: flex;
  align-items: center;

  border: 2px solid #989fdb;
  color: #989fdb;

  ${props =>
    props.isErrored &&
    css`
      border-color: #ff377f;
    `}

  ${props =>
    props.isFocused &&
    css`
      color: #989fdb;
      border-color: #989fdb;
    `}
    
  ${props =>
    props.isFilled &&
    css`
      color: #989fdb;
    `}

  input {
    font-size: 13px;

    flex: 1;
    border: 0;
    background: transparent;
    color: #383e71;
    &::placeholder {
      color: #989fdb;
    }
  }
`;

export const Label = styled.label`
  display: block;
  margin: 0 0 5px 10px;

  font-size: 13px;
  text-transform: uppercase;
`;

export const Error = styled.span`
  display: block;
  margin: 8px 10px 0;

  font-size: 12px;
  color: #ff377f;
`;
