import { useDispatch, useSelector } from 'react-redux';
import Image from 'next/image';
import { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { loginRequest } from '../../store/ducks/auth/actions';
import {
  Container,
  ImageCover,
  Title,
  SubTitle,
} from '../../styles/pages/login';
import Input from '../../components/Input';
import Button from '../../components/Button';
import { RootState } from '../../store/ducks/rootReducer';

const Login = (): JSX.Element => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { isAuthenticated, error } = useSelector((app: RootState) => app.auth);

  useEffect(() => {
    if (isAuthenticated) {
      router.push('/');
    }
  }, [router, isAuthenticated]);

  const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const loginForm = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    dispatch(loginRequest(form));
  };

  const handleOnChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.target;
    setForm({
      ...form,
      [name]: value,
    });
  };

  return (
    <Container>
      <ImageCover>
        <Image src="/login-cover.png" alt="cover" width="1920" height="1080" />
        <div className="cover" />
      </ImageCover>
      <main>
        <div>
          <Title>Olá, seja bem-vindo!</Title>
          <SubTitle>Para acessar a platafarma, faça seu login</SubTitle>
          <form onSubmit={loginForm}>
            <Input
              error={error}
              value={form.email}
              onChange={handleOnChange}
              type="email"
              placeholder="user.name@mail.com"
              name="email"
              label="Email"
            />
            <Input
              value={form.password}
              onChange={handleOnChange}
              placeholder="*******"
              type="password"
              name="password"
              label="Senha"
            />
            <Button type="submit" style={{ marginTop: 20 }}>
              Entrar
            </Button>
          </form>
        </div>
      </main>
    </Container>
  );
};

export default Login;
