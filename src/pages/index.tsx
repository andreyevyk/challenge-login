import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../store/ducks/rootReducer';
import { Welcome } from '../styles/pages/home';

const Home = (): JSX.Element => {
  const { isAuthenticated } = useSelector((app: RootState) => app.auth);
  const router = useRouter();

  useEffect(() => {
    if (!isAuthenticated) {
      router.push('/login');
    }
  }, [router, isAuthenticated]);

  return (
    <Welcome>
      <h1>Seja Bem vindo!</h1>
    </Welcome>
  );
};

export default Home;
