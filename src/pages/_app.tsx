import { AppProps } from 'next/app';
import SEO from '../components/SEO';
import { wrapper } from '../store';
import GlobalStyle from '../styles/global';

function App({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <>
      <GlobalStyle />
      <SEO title="Challenge Login, Front end" />
      <Component {...pageProps} />
    </>
  );
}

export default wrapper.withRedux(App);
