import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box
  }
  body{
    background-color: #FAF5FF;
  }
  body,input, button {
    font: 16px Montserrat, sans-serif ;
    font-weight: 400;
    color: #383e71;
  }
  button {
    cursor: pointer;
  }
`;
