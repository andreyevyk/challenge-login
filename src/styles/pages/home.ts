import styled from 'styled-components';

export const Welcome = styled.div`
  display: flex;
  flex: 1;
  height: 100vh;

  justify-content: center;
  align-items: center;

  h1 {
    font-size: 56px;
    font-weight: 700;
  }
`;
