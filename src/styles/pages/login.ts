import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  background-color: #130525;

  main {
    background-color: #faf5ff;
    width: 45%;
    position: relative;

    display: flex;
    flex-direction: column;

    justify-content: center;
    align-items: center;

    > div {
      width: 300px;
    }

    @media (max-width: 1024px) {
      width: 60%;

      > div {
        zoom: 1.05;
      }
    }

    @media (max-width: 594px) {
      display: flex;
      align-items: center;
      justify-content: center;

      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);

      border-radius: 8px;
      width: 85%;

      padding: 20px 15px 60px;

      > div {
        width: inherit;
      }
      button {
        position: absolute;
        bottom: -50px;
        width: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        box-shadow: none;
      }
    }
  }
`;

export const ImageCover = styled.div`
  height: 100%;
  display: flex;
  flex: 1;
  position: relative;

  img {
    object-fit: cover;
  }

  @media (max-width: 594px) {
    height: 60%;
  }

  div.cover {
    position: absolute;
    top: 0;
    height: 100%;
    width: 100%;
    background: linear-gradient(180deg, rgba(105, 57, 153, 0) 0%, #130525 100%);
  }
`;

export const Title = styled.h1`
  font-weight: 400;
  font-size: 48px;
  color: #383e71;

  @media (max-width: 594px) {
    text-align: center;
    font-size: 30px;
  }
`;

export const SubTitle = styled.h4`
  margin: 15px 0 20px;
  font-weight: 600;
  font-size: 19px;
  color: #989fdb;

  @media (max-width: 594px) {
    text-align: center;
    font-size: 13px;
  }
`;
