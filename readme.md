# Sobre
Aplicação desenvolvida para seletiva da Wiser educação. **Challange Login - Frontend**
# Onde Acessar
Código fonte: [Gitlab](https://gitlab.com/andreyevyk/challenge-login)

Aplicação hospedada na vercel: https://challenge-login-nine.vercel.app/



# Como rodar local
1. Rodar o camando **yarn buil** ou **npm run build**.
2. Fazer uma copia do **.env.example**.
3. Criar um mock, chamado **users**, em um serviço de mock.
4. Colocar no  ***.env.example** NEXT_PUBLIC_API_URL* o endpoint gerado pelo serviço de mock.
5. Rodar o comando **yarn dev**

# Stack Utilizada
* Typescript
* React
* Hooks
* NextJS
* Styled Components
* Redux & Redux Saga
* Axios
* EsLint
* Prettier
* Babel
* Husky
* Lint-staged